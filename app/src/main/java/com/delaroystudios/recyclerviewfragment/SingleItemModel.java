package com.delaroystudios.recyclerviewfragment;

public class SingleItemModel {
    private int imageAtas;
    private String name;


    public SingleItemModel() {
    }

    public SingleItemModel(int imageAtas, String name) {
        this.imageAtas = imageAtas;
        this.name = name;
    }

    public int getImageAtas() {
        return imageAtas;
    }

    public void setImageAtas(int imageAtas) {
        this.imageAtas = imageAtas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}