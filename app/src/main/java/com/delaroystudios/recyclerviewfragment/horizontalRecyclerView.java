package com.delaroystudios.recyclerviewfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class horizontalRecyclerView extends Fragment {

    ArrayList<SectionDataModel> allSampleData;
    ArrayList<SingleItemModel> singleItem = new ArrayList<>();

    public horizontalRecyclerView() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.activity_horizontal_recycler_view, container, false);

        allSampleData = new ArrayList<SectionDataModel>();

        createDummyData();


        RecyclerView my_recycler_view = view.findViewById(R.id.rh_recycler_view);

        my_recycler_view.setHasFixedSize(true);

        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        my_recycler_view.setAdapter(adapter);

        return view;
    }

    private void createDummyData() {
        for (int i = 1; i <= 2; i++) {

            SectionDataModel dm = new SectionDataModel();

            dm.setHeaderTitle("Bagian " + i);


            singleItem.add(new SingleItemModel(R.drawable.round, "World"));
            singleItem.add(new SingleItemModel(R.drawable.buku, "Book"));
            singleItem.add(new SingleItemModel(R.drawable.smile, "Smile"));
            singleItem.add(new SingleItemModel(R.drawable.smile, "Smile"));
            singleItem.add(new SingleItemModel(R.drawable.smile, "Smile"));
            singleItem.add(new SingleItemModel(R.drawable.buku, "Book"));
            singleItem.add(new SingleItemModel(R.drawable.round, "World"));
            singleItem.add(new SingleItemModel(R.drawable.round, "World"));

            dm.setAllItemsInSection(singleItem);

            allSampleData.add(dm);

        }
    }
}
