package com.delaroystudios.recyclerviewfragment.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items {
    @SerializedName("meals")
    private ArrayList<meal> meals;

    public ArrayList<meal> getMeals() {
        return meals;
    }

    public class meal {
        @SerializedName("strMeal")
        private String strMeal;

        @SerializedName("strMealThumb")
        private String strMealThumb;


        public String getStrMealThumb() {
            return strMealThumb;
        }

        public String getStrMeal() {
            return strMeal;
        }
    }


}
