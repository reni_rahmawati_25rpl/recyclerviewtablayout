package com.delaroystudios.recyclerviewfragment.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class categori {
    @SerializedName("categories")
    private ArrayList<categories> categoris;

    public ArrayList<categories> getCategoris() {
        return categoris;
    }

    public class categories {
        @SerializedName("strCategory")
        private String strCategori;

        @SerializedName("strCategoryThumb")
        private String strCategoryThumb;

        public String getStrCategori() {
            return strCategori;
        }

        public String getStrCategoryThumb() {
            return strCategoryThumb;
        }
    }
}
