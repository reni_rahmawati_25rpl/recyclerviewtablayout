package com.delaroystudios.recyclerviewfragment.res;

import com.delaroystudios.recyclerviewfragment.model.Items;
import com.delaroystudios.recyclerviewfragment.model.categori;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("latest.php")
    Call<Items> getData();

    @GET("categories.php")
    Call<categori> getCategori();


}
