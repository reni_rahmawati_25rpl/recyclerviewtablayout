package com.delaroystudios.recyclerviewfragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.delaroystudios.recyclerviewfragment.model.categori;
import com.squareup.picasso.Picasso;

import retrofit2.Response;

public class RecyclerCategoriAdapter extends RecyclerView.Adapter<RecyclerCategoriAdapter.ViewHolder> {
    Response<categori> mCategori;

    public RecyclerCategoriAdapter(Response<categori> response) {
        this.mCategori = response;
    }

    @NonNull
    @Override
    public RecyclerCategoriAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RecyclerCategoriAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.categori_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(mCategori.body().getCategoris().get(position).getStrCategori(), mCategori.body().getCategoris().get(position).getStrCategoryThumb());
    }

    @Override
    public int getItemCount() {
        return mCategori.body().getCategoris().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.namaMakanan);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
                }
            });


        }

        public void setData(String title, String img) {
            textView.setText(title);
            Picasso.get().load(img).into(imageView);
        }
    }
}
