package com.delaroystudios.recyclerviewfragment;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Movie {
    private String title, genre, year;
    private int foto;

    public Movie() {

    }

    public Movie(String title, String genre, String year, int foto) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.foto = foto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}
