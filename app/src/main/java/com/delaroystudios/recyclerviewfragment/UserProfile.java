package com.delaroystudios.recyclerviewfragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserProfile extends AppCompatActivity {

    EditText nama, email, alamat;
    Button save_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        nama = findViewById(R.id.nama);
        email = findViewById(R.id.email);
        alamat = findViewById(R.id.alamat);
        save_data = findViewById(R.id.save_data);


        save_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharePreferences.setData(getApplicationContext(), nama.getText().toString());
                if (!nama.getText().toString().equals("")) {
                    Toast.makeText(UserProfile.this, "Save data success", Toast.LENGTH_SHORT).show();
                    nama.setText("");
                }

                SharePreferences.setEmail(getApplicationContext(), email.getText().toString());
                if (!email.getText().toString().equals("")) {
                    Toast.makeText(UserProfile.this, "Save data success", Toast.LENGTH_SHORT).show();
                    email.setText("");
                }

                SharePreferences.setAlamat(getApplicationContext(), alamat.getText().toString());
                if (!alamat.getText().toString().equals("")) {
                    Toast.makeText(UserProfile.this, "Save data success", Toast.LENGTH_SHORT).show();
                    alamat.setText("");
                }
            }
        });


    }
}
