package com.delaroystudios.recyclerviewfragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.delaroystudios.recyclerviewfragment.model.Items;
import com.squareup.picasso.Picasso;

import retrofit2.Response;

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    Response<Items> mData;

    public RecyclerAdapter(Response<Items> response) {
        this.mData = response;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kiri_list_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setData(mData.body().getMeals().get(i).getStrMeal(), mData.body().getMeals().get(i).getStrMealThumb());
    }

    @Override
    public int getItemCount() {
        return mData.body().getMeals().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.namaMakanan);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
                }
            });


        }

        public void setData(String title, String img) {
            textView.setText(title);
            Picasso.get().load(img).into(imageView);
        }
    }
}